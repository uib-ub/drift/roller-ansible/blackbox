Role Name
=========

A role to build and deploy blackbox, a middleware layer for momayo infrastructure for communication with elasticsearch.


Requirements
------------
roles:
* centos7-system,  worker (with token for read access to git-repo)

additional requirements on host
* java installed


--------------

Variables are set for running blackbox and elasticsearch on same server, with port 9300.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
